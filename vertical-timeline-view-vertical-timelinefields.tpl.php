<?php
/**
 * @file
 * This template includes the vertical_timeline-field view.
 */

/**
 * This template is supposed to display the data in a per row basis.
 * We intentionally leave this empty to give way to
 * views-view-vertical-timeline.tpl.php to print everything.
 */
?>
